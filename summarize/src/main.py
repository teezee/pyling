# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Python 4 Linguists final project implementation

This is the main file of the implemented project.
It provides command line argument checking and
unifies all modules.

The framework consist of the following modules:

abstract            --          abstract base class and functions
syllabify           --          classes for syllabifying
stemmer             --          implements stemming algorithms
project             --          implements the classes stated in project.pdf

see modulename.html or help(module) for documentation

In addition the following files are provided:

stopwords.txt       --      a german stopword list
                            source: http://snowball.tartarus.org/algorithms/german/stop.txt
rotkaeppchen.txt    --      sample corpus
genesis.txt         --      sample corpus

USAGE
-----
python main.py [options]

OPTIONS
-------
-h / --help
    do nothing and print help on stdout

-i 'filename'
    use 'filename' as inputfile
    default: rotkaeppchen.txt

-o 'filename'
    use 'filename' as outputfile for the summary
    default: none

-p
    use porter stemmer for stemming
    if omitted, uses simple stemmer

-a
    use auto syllabifier
    if omitted uses the default procedure
    NOTE: currently NOT supported

-s
    use automatic stopwords identification
    if omitted uses stopwords.txt

-n 'num'
    set the number of significant sentences
    default: 5

-t 'num'
    set the number of significant words
    default: 10

-w 'num'
    set the window for computing significant sentences
    default: 4

EXAMPLES
--------
python -i rotkaeppchen.txt -o output.txt -p -s
    reads rotkaeppchen.txt, uses porter stemmer and auto stopwords
    writes results on stdout and output.txt

pythons -t 20 -w 5 -n 10
    reads default rotkaeppchen.txt
    uses standard stemming and stopwords.txt
    sets number of significant words to 20
    sets number of significant sentences to 10
    writes results to stdout
"""

import abstract as abc
import project as prj
import stemmer as stm
import syllabify as syl
import getopt,sys


def main (argv):
    if argv == []: usage()
    # set defaults
    infile = 'rotkaeppchen.txt'
    outfile = ''
    auto = False
    adv = False
    swfile = 'stopwords.txt'
    threshold = 10
    nrsents = 5
    window = 4
    # check arguments
    try: opts,args = getopt.getopt(argv,"i:o:t:n:w:hpas", ["help"])
    except (getopt.GetoptError): usage()
    for opt, arg in opts:
        if opt == "-i": infile = str(arg)
        elif opt == "-o": outfile = str(arg)
        elif opt == "-p": adv = True
        elif opt == "-a": auto = True
        elif opt == "-s": swfile = None
        elif opt == "-t": threshold = int(arg)
        elif opt == "-n": nrsents = int(arg)
        elif opt == "-n": window = int(arg)
        elif opt in ("-h","--help"): usage()
    # do it
    c = prj.Corpus(infile)
    a = prj.AutoSum(c,nrsents,threshold,swfile,window,auto,adv)
    a.printSummary(outfile)
    return a

def usage():
    s = """USAGE
-----
python main.py [options]

OPTIONS
-------
-h / --help
    do nothing and print help on stdout

-i 'filename'
    use 'filename' as inputfile
    default: rotkaeppchen.txt

-o 'filename'
    use 'filename' as outputfile for the summary
    default: none

-p
    use porter stemmer for stemming
    if omitted, uses simple stemmer

-a
    use auto syllabifier
    if omitted uses the default procedure
    NOTE: currently NOT supported

-s
    use automatic stopwords identification
    if omitted uses stopwords.txt

-n 'num'
    set the number of significant sentences
    default: 5

-t 'num'
    set the number of significant words
    default: 10

-w 'num'
    set the window for computing significant sentences
    default: 4

EXAMPLES
--------
python -i rotkaeppchen.txt -o output.txt -p -s
    reads rotkaeppchen.txt
    uses porter stemmer and auto stopwords
    writes results on stdout and output.txt

pythons -t 20 -w 5 -n 10
    reads default rotkaeppchen.txt
    uses standard stemming and stopwords.txt
    sets number of significant words to 20
    sets number of significant sentences to 10
    writes results to stdout
"""
    print s
    sys.exit(0)
    
if __name__=="__main__":
    a = main(sys.argv[1:])
    
