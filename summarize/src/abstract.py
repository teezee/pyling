# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Abstract Classes

Implements abtract base classes ABC for python.
An interface can be implemented by using Abstract()
as base class and define abstract methods raising
NotImplementedError on call.
"""

__all__ = ["Abstract","abstract","implement"]

class Abstract ():
    """Abstract Base Class"""
    def __init__ (self):
        """Can not be instantiated"""
        raise NotImplementedError("Abstract Base Class cannot be instantiated")

def abstract ():
    raise NotImplementedError("Abstract method must be overridden")

def implement():
    raise NotImplementedError("Method not implemented")
