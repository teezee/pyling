# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import vec, math
'''
this class represents a term.
a term is a word, with his frequency,
and the position in the vector.
the amount of documents it occurs is also stored
'''
class term:

    '''
    init sets wordstem, how many times the stem occurs in all documents and the
    position in the vector. the documentcount shows in how many documents the stem
    occurcs at least one time
    @param wordstem is the stringrepresnetation of the stem
    @param frequency is the number the term occurcs yet
    @param position represents the position in vectors the term stands for
    '''
    def __init__(self, wordstem, frequency, position):
        self.wordstem = wordstem
        self.frequency = frequency        # total frequency of term
        self.position = position          # dimension of term in document vector
        self.documentcount = 1        # number of documents with this term

    # raisedoccount increments the amount of documents where the stem exists
    def raisedoccount(self):
        self.documentcount+=1
        
    # raisefrequency increments the number, the stem exists in all documents
    def raisefrequency(self):
        self.frequency += 1
        
    # str just return a string representation of all fields
    # return string representation of the term
    def __str__(self):
        #return self.wordstem+" : "+str(self.position)+" : "+str(self.frequency)+" : "+str(self.documentcount)
        return self.wordstem

    # repr just return a string representation of all fields
    # return string representation of the term
    def __repr__(self):
        #return self.wordstem+" : "+str(self.position)+" : "+str(self.frequency)+" : "+str(self.documentcount)
        return self.wordstem
    
    def initclusterfrequency(self):
        self.clusterfrequency = 0;
        
    def incclusterfrequency(self):
        self.clusterfrequency += 1

    def settermclusterfrequency(self,tf):
        self.tf = tf



'''
this class represents a document
every document has an ID,
a vector with the amount of words,
and the title of the document
'''
class document:

    '''
    init sets the id, the size of the vector and the title of the document
    @param id is a number thich identifies the document
    @param vectorsize give the actual size of the vector
    @param title is the title of the document
    '''
    def __init__(self, id, vectorsize, title):
        self.id =id
        self.vector = vec.vec()
        self.fillDimension(vectorsize)
        self.title=title
    
    # addelement increase the value of a wordstem in the vector
    # @param position is the position in the vector
    def addElement(self,position):
        self.vector[position]=self.vector[position]+1

    # fillDimension increase the size of the vector till its even with the amount parameter
    # the new dimensions are filled with zeros
    # @param amount is the size, the vector has
    def fillDimension(self,amount):
        self.vector.fill(amount)

    # getDim returns the value of the vector at position "dimension"
    # @param dimension the position in the vector
    # @return value of the requested position
    def getDim(self,dimension):
        return self.vector[dimension]

    # str returns a representation of ID and title of the document
    # return string representation of the document
    def __str__(self):
        return "ID: "+str(self.id)+" Title: "+self.title

    # str returns a representation of ID and title of the document
    # return string representation of the document
    def __repr__(self):
        return "ID: "+str(self.id)+" Title: "+self.title

'''
coef returns the coefficient for the tfidf formula
@param tf is the number the term occurcs in a document
@return the coefficient for the tfidf formula
'''
def coef(tf):
    if (tf==0):
        return 0
    if (tf==1):
        return 1
    if (tf>1 and tf <=5):
        return 1.5
    if (tf>5 and tf <=10):
        return 2
    else:
        return 2.5

'''
tfidf represents the homonymous formula.
@param tf is the amount a word exists in one document 
@param n is the number of documents
@param df is the number of documents the word exists in
@return weight of one element in a vector
'''
def tfidf(tf,n,df):
    return coef(tf)*(math.log10(n)-math.log10(df)+1)
'''
tficf represents the homonymouse formula.
@param tf is the frequency of term in specific cluster
@param c is the number of clusters
@param cf is the number of clusters, the word exists in
@return clusterweight of one element in a vector
'''
def tficf(tf,c,cf):
    return tf*(math.log10(c)-math.log10(cf))

       
