# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import glob
'''
parseDocs reads files from the Reuters Collection. 
it covers the TITLE and BODY part for each News and stores it
the position on docs is the same as in titles for the same News
@param root ist the filedirectory where to find the Documents
@return docs is a list of all Bodys
@return titles is a list of all Titles 
'''
def parseDocs(root):
    titles  = list()
    docs = list()
    for temp in root:
        for file in glob.glob(temp):
            in_file = open(file.replace("\\","//"))
            file_splits = in_file.read()
            file_splits = replaceSGML(file_splits)
            in_file.close()
            docst   = 0
            docend  = 0
            bodyst  = 0
            bodyend = 0
            titlest = 0
            titlend = 0
            docst   = str.find(file_splits,"<REUTERS",docst+1)
            docend  = str.find(file_splits,"</REUTERS>",docst+1)+1
            while (docst > -1):                                         # while new Reuter News exist go on
                if str.find(file_splits,"<BODY>",bodyst+1) < docend:    # a News without BODY part is not relevant
                    titlest = str.find(file_splits,"<TITLE>",docst)
                    titlend = str.find(file_splits,"</TITLE>",docst)+1
                    bodyst = str.find(file_splits,"<BODY>",docst)
                    bodyend = str.find(file_splits,"</BODY>",docst)+1
                    titles.append(replace(file_splits[titlest+7:titlend-1]).lower())     # append title of new document into title list
                    docs.append(replace(file_splits[bodyst+6:bodyend-4]).lower())        # append bodypart of new document into document list
                docst   = str.find(file_splits,"<REUTERS",docst+1)
                docend  = str.find(file_splits,"</REUTERS>",docst+1)+1
    return docs,titles

'''
Special Symbols will be deleted in a String
@param document is a string represents one document.
@return document without special symbols
'''
def replace(document):
    document = document.replace("\"","")
    document = document.replace("\'","")
    document = document.replace("\`","")
    document = document.replace("#","")
    document = document.replace("<","")
    document = document.replace(">","")
    document = document.replace(".","")
    document = document.replace(",","")
    document = document.replace(";","")
    document = document.replace(":","")
    document = document.replace("-"," ")
    document = document.replace("!","")
    document = document.replace("?","")
    document = document.replace("\(","")
    document = document.replace("\)","")
    document = document.replace("\\"," ")
    document = document.replace("  "," ");
    return document

'''
SGML Format Strings will be deleted in a String
@param document is a string represents one document.
@return document without SGML format strings
'''
def replaceSGML(document):
    ampersand = 0;
    semicolon = 0;
    ampersand = str.find(document,"&",ampersand+1)
    semicolon = str.find(document,";",ampersand+1)+1
    while (ampersand > -1):
        subst = document[ampersand:semicolon]
        if (subst == "&lt;"):
            subst = document[ampersand:str.find(document,">",ampersand+1)]                
        document = document.replace(subst,"")
        ampersand = str.find(document,"&",ampersand+1)
        semicolon = str.find(document,";",ampersand+1)+1
    return document    

