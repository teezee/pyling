# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

'''
topicextract.py
from a given document collection, term dictionary and a document clustering
extract cluster content describing topics.
'''
import term, sys

def FindTopics(terms, clustering, documents, topiclength, progress=False):
        
        topics = [[] for i in range(len(clustering))]
        candidates = [{} for i in range(len(clustering))]
        
        if progress: print "calculating new cluster specific term weights",
        for t in terms.values():
            cluster_counter = 0
            tf = [0] * len(clustering)
            t.initclusterfrequency()
            for cluster in clustering:
                for vector in cluster.vectors:
                    doc_id = vector.ref
                    document = documents[doc_id]
                    term_frequency = document.vector[t.position]
                    if term_frequency != 0:
                        tf[cluster_counter] += 1;
                if tf[cluster_counter] != 0:
                    t.incclusterfrequency()
                cluster_counter = cluster_counter + 1

            t.settermclusterfrequency(tf)
            cluster_counter = 0 
            for freq in t.tf:
                if freq != 0:
                    weight = term.tficf(freq, len(clustering), t.clusterfrequency)
                    candidates[cluster_counter][t] = weight
                cluster_counter = cluster_counter + 1
            #if progress: print '.',; sys.stdout.softspace = 0

        if progress: print "!\nsselecting topics",
        for c in range(len(topics)):
                topics[c] = candidates[c].items()
                topics[c].sort(key = lambda x:x[1], reverse = True)
                topics[c] = topics[c][0:topiclength-1:1]
                #if progress: print '.',; sys.stdout.softspace = 0
        if progress: print "!"
        return topics
        
            
    
        
