# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import vec, random, sys

def cluster(vectors):
    return Cluster(vectors)

class Cluster(object):
   '''
   class for cluster representation
   @self.points: a list of vectors
   @self.n: the number of dimensions
   @self.__len__: number of points represented
   @self.centroid: the centroid
   '''
   def __init__(self, vectors):
      self.vectors = []
      self.n = len(vectors[0])
      for v in vectors:
         if len(v) != self.n: raise Exception("Multiple Dimensions Exception")
         if isinstance(v, list): self.vectors.append(vec.Vec().fromlist(v))
         elif isinstance(v, vec.Vec): self.vectors.append(v)
         else: raise Exception("Invalid Input Exception")
      self.__len__ = len(vectors)
      self.centroid = vec.mean(self.vectors)

   def __repr__(self):
      return str(self.vectors)

   def __str__(self):
      return str(self.vectors)

   def __len__(self):
      return len(self.vectors)

   def __getitem__(self, i):
      return self.vectors[i]

   def put(self, v):
      if len(v) != self.n: raise Exception("Multiple Dimensions Exception")
      if isinstance(v, list): self.vectors.append(vec.Vec().fromlist(v))
      elif isinstance(v, vec.Vec): self.vectors.append(v)
      else: raise Exception("Invalid Input Exception")
      self.centroid = vec.mean(self.vectors)
      self.__len__ = len(self.vectors)

   def pop(self, index):
      r = self.vectors.pop(index)
      self.centroid = vec.mean(self.vectors)
      self.__len__ = len(self.vectors)
      return r

   def remove(self, v):
      r = self.vectors.remove(v)
      self.centroid = vec.mean(self.vectors)
      self.__len__ = len(self.vectors)
      return r

   def update(self, vectors):
      self.vectors = vectors
      self.centroid = vec.mean(self.vectors)

   def settopic(self, topic):
       self.topic = topic


#######################
# clustering algorithms
#######################
"""
all clustering algorithms take the following two common arguments:

dist: a distance or similarity function. most common functions are implemented in
      vec.py, but any function that results in comparable values will do
comp: a comparator, used to indicate whether lower values or higher values
      produced by the distance/similarity function mean higher similarity.
      use -1 if lower is better, 1 if higher is better.
      
in general distance based functions like l-metric result in distances, ie the lower, the closer.
similarity functions like the cosine result in similarity, ie the higher, the more similar.
use -1 for distance functions, +1 for similarity functions.
"""

def leader(vectors, t, dist, comp, progress=False):
   '''
   implements the incremental leader algorithm
   @param points: the list of vectors to cluster
   @param t: similarity/distance threshold for forming a new cluster
             if t is 'better' than dist/sim then a new cluster is formed
   @param dist: the distance function to use
   @param comp: the comparator
   @return: a clustering
   '''
   f = max if comp==1 else min 
   clusters = [Cluster([vectors[0]])]
   for v in vectors[1:]:
       distances = []
       for c in clusters:
           distances.append(dist(v, c.centroid))
       if cmp(t,f(distances)) == comp:
           cluster = Cluster([v])
           clusters.append(cluster)
           if progress: print '*',; sys.stdout.softspace = 0
       else:
           cluster = clusters[distances.index(f(distances))]
           cluster.put(v)
           if progress: print '.',; sys.stdout.softspace = 0
   return clusters


def kmeans(points, k, dist, comp, progress=False):
   '''
   implements k-means cluster algorithm
   @param points: list of vectors to cluster
   @param k: number of clusters to find
   @param dist: similarity/distance function to use
   @param comp: the comparator
   @return: a list of clusters
   '''
   # randomly assign k points as initial clusters
   initial = random.sample(points, k)
   clusters = []
   for p in initial: clusters.append(Cluster([p]))
   changed = True
   while changed:
      if progress: print '|',; sys.stdout.softspace = 0
      # a list of points for each cluster
      lists = []
      for i in range(k): lists.append([])
      for p in points:
         # find closest centroid
         cnum = 0
         closest = dist(p, clusters[cnum].centroid)
         for i in range(1,k):
            distance = dist(p, clusters[i].centroid)
            if cmp(distance,closest) == comp:
               closest = distance
               cnum = i
         # add point to cluster's list
         lists[cnum].append(p)
      # update each Cluster with corresponding list
      notchanged = 0
      for i in range(k):
         if clusters[i].vectors == lists[i]:
            notchanged +=1
            if progress: print '.',; sys.stdout.softspace = 0
         else:
            clusters[i].update(lists[i])
            if progress: print '*',; sys.stdout.softspace = 0
      # check break conditions
      if notchanged == k: changed = False
   return clusters


def agglomorate(points, link, t, dist, comp, progress=False):
   '''
   implements the agglomoration algorithm
   @param points: list of vectors to cluster
   @param linkage: the linkage method to use: 's','c','a'
   @param t: agglomoration threshold, break condition for merging clusters
             if t is better than best dist/sim then stop merging
   @param dist: similarity/distance function to use
   @param comp: the comparator
   @return: a list of clusters
   '''
   # initialize with singleton Clusters, one for each Point
   clusters = []
   for p in points: clusters.append(Cluster([p]))
   while True:
      if progress: print '.',; sys.stdout.softspace = 0
      dm = distanceMatrix(clusters, link, dist, comp)
      # find the closest clusters, similarity must be higher, distance lower
      best_key = dm.keys()[0]
      best_dist = dm[best_key]
      for key in dm.keys():
         if cmp(dm[key],best_dist) == comp:
            best_key = key
            best_dist = dm[key]
      # compare to threshold and merge or break
      if cmp(t,best_dist) == comp: break
      else:
         c0, c1 = clusters[best_key[0]], clusters[best_key[1]]
         c = Cluster(c0.vectors + c1.vectors)
         clusters.remove(c0)
         clusters.remove(c1)
         clusters.append(c)
         if progress: print '*',; sys.stdout.softspace = 0
      if len(clusters) == 1: break
   return clusters

def linkage(c0, c1, link, dist, comp):
   if link == 'a': return dist(c0.centroid,c1.centroid)
   if link == 's': cp = comp
   if link == 'c': cp = -comp
   d = dist(c0.vectors[0], c1.vectors[0])
   for i in c0.vectors:
      for j in c1.vectors:
         distance = dist(i,j)
         if cmp(distance, d) == cp: d = distance
   return d

def distanceMatrix(clusters, link, dist, comp):
    dm = dict()
    l = len(clusters)
    for i in range(l):
        for j in range(l):
            if j == i: break
            dm[(i,j)] = linkage(clusters[i],clusters[j],link,dist,comp)
    return dm


def aggregate(A, B, t, dist, comp, progress=False):
   """
   implements the cluster aggregation algorithm as proposed by
   Hanan Ayad and Mohamed S. Kamel. Topic discovery from text
   using aggregation of different clustering methods. 
   can take two clusterings and produces a combined clustering
   @param A:      clustering A
   @param B:      clustering B
   @param t:      aggregation threshold
   @param dist:   distance/similarity function to use
   @param comp:   comparator for dist
   """
   G = []
   processed = 0
   for i in range(len(A)):
      if progress: print 'a',; sys.stdout.softspace = 0
      a = frozenset(A[i])
      W = a
      for j in range(len(B)):
         if progress: print 'b',; sys.stdout.softspace = 0
         if (processed >> j) & 1: continue
         b = frozenset(B[j])
         I = a & b
         if I != set():                        # a intersects with b
            dAB = a-b
            dBA = b-a
            if dAB == set():                    # a is subset of b
               if dBA == set():                 # a == b
                  if progress: print '==',; sys.stdout.softspace = 0
                  if I not in G: G.append(I)
               elif dBA != set():                               # b is superset of a, ie a completely in b
                  if progress: print '=!',; sys.stdout.softspace = 0
                  newCluster = [] 
                  c0 = Cluster(list(I)).centroid
                  for p in dBA:
                     if cmp(dist(p, c0), t) in [0, comp]: W |= frozenset([p])
                     else: newCluster.append(p)
                  if W not in G: G.append(W)
                  if frozenset(newCluster) not in G and newCluster!=[]: G.append(frozenset(newCluster))
            elif dAB != set():                                  # a-b != set(), 
               if dBA != set():                 # b-a != set(), a and b have some common points
                  if progress: print '!!',; sys.stdout.softspace = 0
                  c1 = Cluster(list(dAB)).centroid
                  c2 = Cluster(list(dBA)).centroid
                  if cmp(dist(c1,c2), t) in [0, comp]:
                     for p in dBA: W |= frozenset([p])
                     if W not in G: G.append(W)
                  else:
                     for p in I:
                        s1 = dist(p,c1)
                        s2 = dist(p,c2)
                        if cmp(s1,s2)==comp:
                           dAB |= frozenset([p])
                        elif cmp(s2,s1)==comp:
                           dBA |= frozenset([p])
                           W ^= frozenset([p])
                        elif s1==s2:
                           dAB |= frozenset([p])
                           dBA |= frozenset([p])
                     if dAB not in G: G.append(dAB)
                     if dBA not in G: G.append(dBA)
               elif dBA == set():                               # b-a == set(), b completly in a
                  if progress: print '=!',; sys.stdout.softspace = 0
                  newCluster = []
                  c0 = Cluster(list(I)).centroid
                  for p in dAB:
                     if cmp(dist(p,c0), t)==-comp:
                        newCluster.append(p)
                        W ^= frozenset([p])
                  if W not in G and W!=set(): G.append(W)
                  if frozenset(newCluster) not in G and newCluster!=[]: G.append(frozenset(newCluster))
            processed |= 1 << j
   G = [Cluster(list(i)) for i in G]
   return G


def aggregate2(A, B, t, dist, comp, progress=False):
   """
   an alternative implementation of the cluster aggregation algorithm as proposed by
   Hanan Ayad and Mohamed S. Kamel. Topic discovery from text
   using aggregation of different clustering methods. 
   can take two clusterings and produces a combined clustering
   @param A:      clustering A
   @param B:      clustering B
   @param t:      aggregation threshold
   @param dist:   distance/similarity function to use
   @param comp:   comparator for dist
   """
   G = []
   processed = 0
   for i in range(len(A)):
      if progress: print 'a',; sys.stdout.softspace = 0
      a = frozenset(A[i])
      for j in range(len(B)):
         if progress: print 'b',; sys.stdout.softspace = 0
         if (processed >> j) & 1: continue
         b = frozenset(B[j])
         I = a & b
         if I != set():
            dAB = a-b
            dBA = b-a
            # case 0 a == b
            if a == b:
                if progress: print '=',; sys.stdout.softspace = 0
                if I not in G: G.append(I)
            # case 1 a <= b xor b <= a
            if (a<b) ^ (b<a):
                if progress: print '^',; sys.stdout.softspace = 0
                J = frozenset([]) 
                c0 = Cluster(list(I)).centroid
                D = dAB if dAB != set() else dBA
                for p in D:
                    if cmp(dist(p, c0), t) in [0, comp]:
                        I ^= frozenset([p])
                    else:
                        J ^= frozenset([p])
                if I not in G: G.append(I)
                if J != set() and J not in G: G.append(J)
            # case 2 a-b
            if not a<=b and not b<=a:
                if progress: print '!',; sys.stdout.softspace = 0
                c1 = Cluster(list(dAB)).centroid
                c2 = Cluster(list(dBA)).centroid
                if cmp(dist(c1,c2), t) in [0, comp]:
                    uni = a | b
                    if uni not in G: G.append(uni)
                else:
                    for p in I:
                        s1 = dist(p,c1)
                        s2 = dist(p,c2)
                        if cmp(s1,s2)==comp:
                           dAB |= frozenset([p])
                        elif cmp(s2,s1)==comp:
                           dBA |= frozenset([p])
                        elif s1==s2:
                           dAB |= frozenset([p])
                           dBA |= frozenset([p])
                        if dAB not in G: G.append(dAB)
                        if dBA not in G: G.append(dBA)
            processed |= 1 << j
   G = [Cluster(list(i)) for i in G]
   return G

"""
prunes overlapping clusters and remove duplicate vectors
"""
def prune(vectors,clustering,dist,comp):
    for v in vectors:
        c1 = []
        for c in clustering:
            if v in c.vectors:
               if c1 == []:
                   c1 = c
               else:
                   if cmp(dist(v, c1.centroid),dist(v, c.centroid)) == comp:
                       c.remove(v)
                   else:
                       c1.remove(v)
                       c1 = c
    return clustering
                    
                    
                
                    


#######################
# helper methods
#######################

def threshold(points, a, dist):
   """
   calculates a stopping threshold based on harmonised average distances
   suitable for agglomorative clustering and aggregation
   @param points: the points
   @param a: an arbitrary coefficient 0 < a <= 1
   @param dist: distance function to use
   @return: the calculated threshold t
   """
   t, N = 0, len(points) 
   den = float((N**2-N)/2)
   for i in points:
      for j in points:
         if i==j: break
         t += a*(dist(i,j)/den)
   return t


#######################
# main
#######################

def main():
   nump,n,lower,upper = 10,2,-10,10
   points = []
   print "\nPOINTS\n"
   for i in range(nump):
      invalid = True
      while invalid:
         v = vec.randomVec(n,lower,upper)
         if v.norm() != 0 and v not in points:
            points.append(v)
            print str(points[i]) + ",",
            invalid = False
   print ""

   print "\nLEADER ALGORITHM\n"
   t = 0.65
   print "Cosine Similarity, t = " + str(t),
   cleader0 = leader(points, t, vec.cos, 1, True)
   print ""
   for c in cleader0:
      print "C:" + str(c)
   t = 7
   print "Euclidean Distance, t = " + str(t),
   cleader1 = leader(points, t, vec.euclid, -1, True)
   print ""
   for c in cleader1:
      print "C:" + str(c)

   print "\nK-MEANS\n"
   k = 4
   print "Cosine Similarity",
   ckmeans0 = kmeans(points,k,vec.cos,1,True)
   print ""
   for c in ckmeans0:
      print "C:" + str(c)

   print "Euclidean Distance",
   ckmeans1 = kmeans(points,k,vec.euclid,-1,True)
   print ""
   for c in ckmeans1:
      print "C:" + str(c)

   print "\nAGGLOMORATIVE\n"
   t = threshold(points, .4, vec.cos)
   link = 's'
   print "Cosine Similarity, " + link + ", t = " + str(t),
   cagglo0 = agglomorate(points,link,t,vec.cos,1, True)
   print ""
   for c in cagglo0:
      print "C:" + str(c)
   t = threshold(points, .5, vec.euclid)
   link = 'c'
   print "Euclidean Distance, " + link + ", t = " + str(t),
   cagglo1 = agglomorate(points,link,t,vec.euclid,-1, True)
   print ""
   for c in cagglo1:
      print "C:" + str(c)

   print "\nAGGREGATION\n"
   t = threshold(points, .9, vec.euclid)
   combined = aggregate(ckmeans1, cagglo1, t, vec.euclid, -1, True)
   print ""
   for c in combined:
      print "C:" + str(c)

   print "\nAGGREGATION2\n"
   combined2 = aggregate2(ckmeans1, cagglo1, t, vec.euclid, -1, True)
   print ""
   for c in combined2:
      print "C:" + str(c)

   print "\nPRUNE\n"
   pruned = prune(points,combined2, vec.euclid, -1)
   for c in pruned:
      print "C:" + str(c)
    
   return points, [ cleader0, cleader1, ckmeans0, ckmeans1, cagglo0, cagglo1 ], combined

if __name__ == "__main__":
   points, clustering, combined = main()
