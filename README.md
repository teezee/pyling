FAIR LICENSE

Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.


# PYLING - Python for Linguists

This is a collection of python projects that perform different linguistics tasks. The projects have been implemented as part of seminars for computer linguists by multiple authors. Therefor, the implementations and documentations are not very professional and lack decent style and design. They are released to the public domain with the hope they might prove useful for somebody.

The structure of the projects is similar and as follows:

* src
  The source code of the project.

* doc
  Project documentation, either in latex and compiled, or in pydoc (html).

* res
  Additional resources, like a stop word list and example documents.

## TOPICEX - Topic Discovery From Text By Cluster Aggregation In Python

This projects implements different clustering algorithms as well as cluster aggregation as proposed by [Ayad2002Topic] to identify similar texts (corpora) and extract a common topic for each cluster found. The cluster algorithms can be used with a variety of similarity / distance functions. For an overview and documentation see topicex/doc/paper.pdf.

## SUMMARIZE - Automatic text Summarization in Python

_Summarize_ reads a german text file (Corpus), performs basic text analysis (stop words, stemming, tokenization, term frequency) and automatically generates a summary of the text which is then written to an output file. Different stemming algorithms are implemented like Porter. The summarize algorithm is inspired by [Luhn1958Automatic].

## References 

[Ayad2002Topic]: "Hanan Ayad and Mohamed S. Kamel; Topic Discovery from Text Using Aggregation of Different Clustering Methods"

[Luhn1958Automatic]: "H.P. Luhn; The Automatic Creation of Literature Abstracts"

